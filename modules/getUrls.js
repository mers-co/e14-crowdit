'use strict';
process.env.UV_THREADPOOL_SIZE = 128;

const request = require('request-promise');
const cheerio = require('cheerio');
const _ = require('lodash');
const http = require('http');

http.globalAgent.maxSockets = 1000;


const low = require('lowdb')
const FileSync = require('lowdb/adapters/FileSync')

const adapter = new FileSync('db.json')
const db = low(adapter)

const apiUrl = 'https://visor.e14digitalizacion.com/e14';
const token = '1241d668dffd9f819963e438b4e760c6678a75d62ebc9bd64f6c8810aa23c1dd';
const cookie = '__cfduid=d41c5264e54c79ac0a2fcf6a0187dd0bd1527634876; SESSION=di76tfpm65u11hg3c13cabkmm6; __cflb=55221073'

const firstOnly = false;

//This will make us look like a browser :D
const headers = {
    cookie,
    accept: '*/*',
    // 'accept-encoding': 'gzip, deflate, br',
    'accept-language': 'en-US,en;q=0.9',
    'cache-control': 'no-cache',
    origin: 'https://visor.e14digitalizacion.com',
    pragma: 'no-cache',
    referer: 'https://visor.e14digitalizacion.com/',
    'user-agent': 'Mozilla/5.0 (Windows NT 6.3; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/65.0.3325.181 Safari/537.36 OPR/52.0.2871.64',
    'x-requested-with': 'XMLHttpRequest'
}

const timeout = 30000;
const defaultDelay = 0;
let baseDelay = defaultDelay;

Number.prototype.pad = function (size) {
    let s = String(this);
    while (s.length < (size || 2)) { s = "0" + s; }
    return s;
}

//TODO: DRY!
let saveDepartments = (departments = []) => {
    if (db.get(`departments`).value() == undefined) {
        db.set(`departments`, {}).write();
    }
    departments = [].concat(departments);
    _.forEach(departments, x => {
        let dbValue = db.get(`departments.${x.id}`).value();
        if (dbValue == undefined) {
            db.set(`departments.${x.id}`, { name: x.name }).write();
        }
    })
}

let saveCities = (departmentId, cities) => {
    let department = db.get(`departments.${departmentId}`);
    if (db.get(`departments.${departmentId}.cities`).value() == undefined) {
        db.set(`departments.${departmentId}.cities`, {}).write();
    }
    _.forEach(cities, x => {
        if (db.get(`departments.${departmentId}.cities.${x.id}`).value() == undefined) {
            db.set(`departments.${departmentId}.cities.${x.id}`, { name: x.name }).write();
        }
    })
}

let saveZones = (departmentId, cityId, zones) => {
    let city = db.get(`departments.${departmentId}.cities.${cityId}.zones`);
    if (db.get(`departments.${departmentId}.cities.${cityId}.zones`).value() == undefined) {
        db.set(`departments.${departmentId}.cities.${cityId}.zones`, {}).write();
    }
    _.forEach(zones, x => {
        if (db.get(`departments.${departmentId}.cities.${cityId}.zones.${x.id}`).value() == undefined) {
            db.set(`departments.${departmentId}.cities.${cityId}.zones.${x.id}`, { name: x.name }).value();
        }
    })

    db.write();
}

let savePlaces = (departmentId, cityId, zoneId, places) => {
    let city = db.get(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places`);
    if (db.get(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places`).value() == undefined) {
        db.set(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places`, {}).write();
    }
    _.forEach(places, x => {
        if (db.get(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places.${x.id}`).value() == undefined) {
            db.set(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places.${x.id}`, { name: x.name }).value();
        }
    })
    db.write();
}

let saveStands = (departmentId, cityId, zoneId, placeId, stands) => {
    let city = db.get(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places.${placeId}.stands`);
    if (db.get(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places.${placeId}.stands`).value() == undefined) {
        db.set(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places.${placeId}.stands`, {}).write();
    }
    _.forEach(stands, x => {
        if (db.get(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places.${placeId}.stands.${x.id}`).value() == undefined) {
            db.set(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places.${placeId}.stands.${x.id}`, { name: x.name, uri: x.uri }).value();
        }
    })
    db.write();
}

let parseDepartments = (departments) => {
    let $ = cheerio.load(departments);
    let options = []
    $('option').each(function (i, elem) {
        let text = $(this).text();
        text = text.substring(text.indexOf('- ') + 2, text.indexOf(' ('))
        options[i] = { id: $(this).attr('value'), name: text }
    });
    return options
}

let parseCities = (cities) => {
    let $ = cheerio.load(cities);
    let options = []
    $('option').each(function (i, elem) {
        let text = $(this).text();
        text = text.substring(text.indexOf('- ') + 2)
        options[i] = { id: $(this).attr('value'), name: text }
    });
    return options
}

let parseZones = (zones) => {
    let $ = cheerio.load(zones);
    let options = []
    $('option').each(function (i, elem) {
        let text = $(this).text();

        if (Number.parseInt($(this).attr('value')) < 0) {
            return null;
        } else {
            options[i] = { id: $(this).attr('value'), name: text }
        }
    });
    return options.filter(x => x != undefined)
}

let parsePlaces = (places) => {
    let $ = cheerio.load(places);
    let options = []
    $('option').each(function (i, elem) {
        let text = $(this).text();
        text = text.substring(text.indexOf('- ') + 2)
        options[i] = { id: Number.parseInt($(this).attr('value')), name: text }
    });
    return options.filter(x => x != undefined)
}

let parseStandFiles = (filesTable) => {
    let $ = cheerio.load(filesTable);
    let files = []
    $('td').each(function (i, elem) {
        let name = $(this).children('a').text();
        let uri = $(this).children('a').attr('href');
        let id = Number.parseInt(name.substr(name.length - 3));
        if (uri !== undefined)
            files.push({
                name, uri, id
            })
    })
    return files
}

let promiseDelay = function (val) {
    return new Promise((resolve, reject) => {
        setTimeout(() => {
            return resolve(val);
        }, baseDelay)
    })
}

let getDepartments = () => {
    if (db.get('departments').value() != undefined) {
        return Promise.resolve(db.get('departments').cloneDeep().value());
    }
    return request.post(apiUrl, {
        headers, timeout, formData: {
            accion: 'cargar_departamentos_select',
            corp_activo: 'presidente',
            token: token
        }
    }).then(response => {
        return parseDepartments(response);
    }).catch(err => {
        if (err.statusCode) {
            console.log('Error', err.statusCode);
        } else {
            console.log('Error', err);
        }
    }).then(departments => {
        saveDepartments(departments);
        return db.get('departments').cloneDeep().value();
    })
}

let getCities = (departmentId, defered = false, postProcess) => {
    if (db.get(`departments.${departmentId}.cities`).value() != undefined) {
        return Promise.resolve(db.get(`departments.${departmentId}.cities`).cloneDeep().value());
    }
    let postRequest = request.post(apiUrl, {
        headers, timeout, formData: {
            accion: 'cambiar_departamento',
            corp_activo: 'presidente',
            dep_activo: Number.parseInt(departmentId).pad(2),
            token: token
        }
    })

    let process = (requestPromise) => {
        return requestPromise.then(response => {
            return parseCities(response);
        }).then(cities => {
            saveCities(Number.parseInt(departmentId), cities);
            cities = db.get(`departments.${departmentId}.cities`).cloneDeep().value();
            if (postProcess) {
                console.log('Post Process Cities');
                return postProcess(cities);
            }
            return cities;
        }).catch(err => {
            if (err.statusCode) {
                console.log('Error', err.statusCode);
            } else {
                console.log('Error', err);
            }
            return Promise.reject(err);
        })
    }

    if (!defered) {
        return process(postRequest);
    }

    return Promise.resolve({
        promise: postRequest,
        process
    })
}

let getZones = (departmentId, cityId, defered = false, postProcess) => {
    if (db.get(`departments.${departmentId}.cities.${cityId}.zones`).value() != undefined) {
        return Promise.resolve(db.get(`departments.${departmentId}.cities.${cityId}.zones`).cloneDeep().value());
    }
    let postRequest =
        request.post(apiUrl, {
            headers, timeout, formData: {
                accion: 'cambiar_municipio',
                corp_activo: 'presidente',
                dep_activo: Number.parseInt(departmentId).pad(2),
                mun_activo: Number.parseInt(cityId).pad(3),
                token: token
            }
        })

    let process = (requestPromise) => {
        return requestPromise.then(zones => {
            return parseZones(zones);
        }).then(zones => {
            // console.log(zones)
            saveZones(Number.parseInt(departmentId), Number.parseInt(cityId), zones);
            zones = db.get(`departments.${departmentId}.cities.${cityId}.zones`).cloneDeep().value();
            if (postProcess) {
                return postProcess(zones)
            }
            return zones;
        }).catch(err => {
            if (err.statusCode) {
                console.log('Error', err.statusCode);
            } else {
                console.log('Error', err);
            }
            return Promise.reject(err);
        })
    }
    if (!defered) {
        return process(postRequest);
    }
    return Promise.resolve({
        promise: postRequest,
        process
    })
}

let getPollingPlaces = (departmentId, cityId, zoneId, defered = false, postProcess) => {
    let postRequest = request.post(apiUrl, {
        headers, timeout, formData: {
            accion: 'cambiar_zona',
            corp_activo: 'presidente',
            dep_activo: Number.parseInt(departmentId).pad(2),
            mun_activo: Number.parseInt(cityId).pad(3),
            zona_activo: Number.parseInt(zoneId).pad(3),
            token: token
        }
    })

    let process = (requestPromise) => {
        return requestPromise.then(places => {
            return parsePlaces(places)
        }).then(places => {
            console.log(departmentId, cityId, zoneId, places.length)
            savePlaces(Number.parseInt(departmentId), Number.parseInt(cityId), Number.parseFloat(zoneId), places);
            places = db.get(`departments.${departmentId}.cities.${cityId}.zones.${zoneId}.places`).cloneDeep().value();
            if (postProcess) {
                return postProcess(places)
            }
            return places;
        }).catch(err => {
            if (err.statusCode) {
                console.log('Error', err.statusCode);
            } else {
                console.log('Error', err);
            }
            return Promise.reject(err);
        })
    }

    if (!defered) {
        return process(postRequest);
    }

    return Promise.resolve({
        promise: postRequest,
        process
    })
}

let getPollingStand = (departmentId, cityId, zoneId, placeId, defered = false, postProcess) => {
    let postRequest = request.post(apiUrl, {
        headers, timeout, forever: true, formData: {
            accion: 'cargar_mesas',
            corp_activo: 'presidente',
            dep_activo: Number.parseInt(departmentId).pad(2),
            mun_activo: Number.parseInt(cityId).pad(3),
            zona_activo: Number.parseInt(zoneId).pad(3),
            pues_activo: Number.parseInt(placeId).pad(2),
            token: token
        }
    })

    let process = (requestPromise) => {
        console.log('getPollingStand', departmentId, cityId, zoneId, placeId)

        return requestPromise.then(stands => {
            return parseStandFiles(stands);
        }).then(stands => {
            saveStands(departmentId, cityId, zoneId, placeId, stands);
            return stands;
        }).catch(err => {
            if (err.statusCode) {
                console.log('Error', err.statusCode);
            } else {
                console.log('Error', err.toString());
            }
            return Promise.reject(err);
        })

    }

    if (!defered) {
        return process(postRequest);
    }

    return Promise.resolve({
        promise: postRequest,
        process
    })

}

let promises = [];


let missingPaths = [];
_.forIn(db.get('departments').cloneDeep().value(), (department, departmentId) => {
    if (!_.has(department, 'cities')) {
        missingPaths.push({
            department: departmentId
        })
    }
    _.forIn(department.cities, (city, cityId) => {
        if (!_.has(city, 'zones')) {
            console.log('City Missing Zones', city.name)
            missingPaths.push({
                department: departmentId,
                city: cityId
            })
        }
        _.forIn(city.zones, (zone, zoneId) => {
            if (!_.has(zone, 'places') && !(_.startsWith(zoneId,'0') && zoneId.length == 2 )) {
                console.log('Zone Missing places',city.name, zoneId)
                missingPaths.push({
                    department: departmentId,
                    city: cityId,
                    zone: zoneId
                })
            }

            _.forIn(zone.places, (places, placeId) => {
                if (!_.has(places, 'stands')) {
                    console.log('Places Missing stands', city.name, zoneId)

                    missingPaths.push({
                        department: departmentId,
                        city: cityId,
                        zone: zoneId,
                        place: placeId
                    })
                }
            })
        })
    })
})

console.log(missingPaths.length, _.first(missingPaths));

// return;
_.forEach(missingPaths, missing => {
    if (_.has(missing, 'place')) {
        if (promises.length < 100) {
            promises.push(
                getPollingStand(missing.department, missing.city, missing.zone, missing.place, true)
            );
        }
    }else if(_.has(missing,'zone')){
        if (promises.length < 100) {
            promises.push(
                getPollingPlaces(missing.department, missing.city,missing.zone,true)
            );
        }
    }
})

// if (_.has(firt, 'place')) {
//     getPollingStand(firt.department, firt.city, firt.zone, firt.place)
// } else if (_.has(firt, 'zone')) {
//     getPollingPlaces(firt.department, firt.city, firt.zone);
// }

/*
getDepartments()
    .then(departments => {
        let departmentIx = 0;
        _.forIn(departments, (department, departmentId) => {
            // console.log(x.name, k);
            if (_.has(department, 'cities') || (departmentIx > 0 && firstOnly)) {
                return;
            }

            promises.push(
                getCities(departmentId, true, cities => {
                    let citiyIx = 0;
                    _.forIn(cities, (city, cityId) => {
                        if (_.has(city, 'zones') || (citiyIx > 0 && firstOnly)) {
                            return;
                        }
                        promises.push(
                            getZones(departmentId, cityId, true, zones => {
                                let zoneIx = 0;
                                _.forIn(zones, (zone, zoneId) => {
                                    if (_.has(zone, 'places') || (zoneIx > 0 && firstOnly)) {
                                        return;
                                    }
                                    promises.push(
                                        getPollingPlaces(departmentId, cityId, zoneId, true, places => {
                                            let placesIx = 0;
                                            _.forIn(places, (place, placeId) => {
                                                if (_.has(place, 'stands') || (placesIx > 0 && firstOnly)) {
                                                    return;
                                                }
                                                promises.push(
                                                    getPollingStand(departmentId, cityId, zoneId, placeId)
                                                )
                                            })
                                        })
                                    )
                                    zoneIx++;
                                })
                            })
                        )
                        citiyIx++;
                    })
                    return Promise.resolve();
                })
            )
            departmentIx++;

        })
    })
    .then(() => {
        console.log('Intial Promises:', promises.length);
        return nextPromise();
    })
*/
var count = 0;

const nextPromise = () => {
    count++;
    console.log(count, ' *', promises.length, baseDelay);
    if (promises.length > 0) {
        let currentPromise = promises.shift();

        return currentPromise.then(r => {
            baseDelay = defaultDelay;
            if (_.has(r, 'promise')) {
                return r.process(r.promise)
                    .catch(err => {
                        console.log('ERR');
                        baseDelay = 10000;
                        promises.unshift(Promise.resolve(r));
                    })

            }
            console.log([].concat(r).length)
            return;
        }).then(promiseDelay).then(nextPromise).catch(err => {
            console.error('error');
            promises.unshift(Promise.resolve(currentPromise));
            baseDelay = 5000;
            return;
        })
    } else {
        return Promise.resolve();
    }
}

return nextPromise();


